using System.ComponentModel.DataAnnotations;

namespace EQBank.Core.Models
{
    public class GlobalLimitRequestParameters
    {
        public string rid { get; set; }
        public string productType { get; set; }
        public string dailyLimit { get; set; }
        public string weeklyLimit { get; set; }
        public string monthlyLimit { get; set; }
        public string yearlyLimit { get; set; }
        public string schemeCode { get; set; }
        public string channel { get; set; }
        public string countryCode { get; set; }
        public string currency { get; set; }
        public string bankCode { get; set; }
    }
}
