using System.ComponentModel.DataAnnotations;

namespace EQBank.Core.Models
{
    public class LimitRequestParameters
    {
        [Required]
        public string rid { get; set; }
        [Required]
        public string accountId { get; set; }
        [Required]
        public string schemeCode { get; set; }
        [Required]
        public string channel { get; set; }
        [Required]
        public string countryCode { get; set; }
        [Required]
        public string currency { get; set; }
        [Required]
        public string bankCode { get; set; }
    }
}
