using System.ComponentModel.DataAnnotations;

namespace EQBank.Core.Models
{
    public class Limit : LimitBase
    {
        [Required]
        public string dailyLimit { get; set; }
        [Required]
        public string weeklyLimit { get; set; }
        [Required]
        public string monthlyLimit { get; set; }
        [Required]
        public string yearlyLimit { get; set; }
        [Required]
        public string tansactionLimit { get; set; }
    }
}
