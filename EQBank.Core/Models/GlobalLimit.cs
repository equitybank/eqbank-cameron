using System.ComponentModel.DataAnnotations;

namespace EQBank.Core.Models
{
    public class GlobalLimit
    {
        [Key]
        public string rid { get;set; }
        [Required]
        public string productType { get; set; }
        [Required]
        public string dailyLimit { get; set; }
        [Required]
        public string weeklyLimit { get; set; }
        [Required]
        public string monthlyLimit { get; set; }
        [Required]
        public string yearlyLimit { get; set; }
        [Required]
        public string schemeCode { get; set; }
        [Required]
        public string channel { get; set; }
        [Required]
        public string countryCode { get; set; }
        [Required]
        public string currency { get; set; }
        [Required]
        public string bankCode { get; set; }
    }
}
