using Microsoft.EntityFrameworkCore;
using EQBank.Core.Models;

namespace EQBank.Core.DBO
{
    public class TransactLimitContext : DbContext
    {
        public TransactLimitContext(DbContextOptions<TransactLimitContext> options)
            : base(options)
        {
        }

        public DbSet<Limit> Limits { get; set; }
        public DbSet<TransactionLimit> TransactionLimits { get; set; }
        public DbSet<GlobalLimit> GlobalLimits { get; set; }
    }
}
