using Microsoft.Extensions.DependencyInjection;
using EQBank.Services.Contracts;

namespace EQBank.Services.DI
{
    public static class ServiceModule
    {
        public static void ConfigureServiceModule(this IServiceCollection services)
        {
            services.AddScoped<IGlobalLimitService, GlobalLimitService>();
            services.AddScoped<ILimitService, LimitService>();
        }
    }
}