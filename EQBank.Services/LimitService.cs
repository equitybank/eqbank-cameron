using EQBank.Services.Contracts;
using EQBank.Core.Models;
using EQBank.Core.DBO;
using System;
using System.Linq;

namespace EQBank.Services
{
    public class LimitService : ILimitService
    {
        private readonly TransactLimitContext _context;

        public LimitService(TransactLimitContext context)
        {
            _context = context;
        }

        // Create transaction Limit - API Specification
        public object CreateLimit(TransactionLimit transactionLimit)
        {
            var status = new Status()
            {
                code = "0",
                description = "success"
            };

            var ret = new
            {
                dailyLimit = transactionLimit.dailyLimit,
                weeklyLimit = transactionLimit.weeklyLimit,
                monthlyLimit = transactionLimit.monthlyLimit,
                yearlyLimit = transactionLimit.yearlyLimit,
                transactionLimit = transactionLimit.transactionLimit
            };

            _context.TransactionLimits.Add(transactionLimit);
            _context.SaveChanges();

            // Build response
            var success = new
            {
                transactionLimit.rid,
                status,
                ret
            };

            return success;
        }

        //Get Transaction Limit - API Specification
        public object GetTransactionLimit(LimitRequestParameters transactionLimit)
        {
            var status = new Status()
            {
                code = "0",
                description = "success"
            };

            var limit = _context.TransactionLimits.FirstOrDefault(p => p.accountId == transactionLimit.accountId);

            if (limit == null)
            { // Throw 404
                status = new Status()
                {
                    code = "1",
                    description = "not found"
                };

                return status;
            }

            var ret = new
            {
                dailyLimit = limit.dailyLimit,
                weeklyLimit = limit.weeklyLimit,
                monthlyLimit = limit.monthlyLimit,
                yearlyLimit = limit.yearlyLimit,
                transactionLimit = limit.transactionLimit
            };

            // Build response
            var success = new
            {
                transactionLimit.rid,
                status,
                ret
            };

            return success;
        }
    }
}