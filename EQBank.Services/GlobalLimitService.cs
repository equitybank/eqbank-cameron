using EQBank.Services.Contracts;
using EQBank.Core.Models;
using EQBank.Core.DBO;

namespace EQBank.Services
{
    public class GlobalLimitService : IGlobalLimitService
    {
        private readonly TransactLimitContext _context;

        public GlobalLimitService(TransactLimitContext context)
        {
            _context = context;
        }

        // Create Global Limit - API Specification
        public object SaveGlobalLimit(GlobalLimitRequestParameters requestParameters)
        {
            var status = new Status()
            {
                code = "0",
                description = "success"
            };

            var globalLimit = new GlobalLimit
            {
                rid = requestParameters.rid
            };

            var ret = new
            {
                dailyLimit = requestParameters.dailyLimit,
                weeklyLimit = requestParameters.weeklyLimit,
                monthlyLimit = requestParameters.monthlyLimit,
                yearlyLimit = requestParameters.yearlyLimit
            };

            _context.GlobalLimits.Add(globalLimit);
            _context.SaveChanges();

            // Build response
            var success = new
            {
                globalLimit.rid,
                status,
                ret
            };

            return success;
        }
    }
}
