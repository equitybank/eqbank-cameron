using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EQBank.Models;
using System.Net.Http;
using EQBank.Services;
using EQBank.Services.Contracts;
using EQBank.Core.Models;
using EQBank.Core.DBO;

namespace EQBank.Controllers
{
    [Route("v1")]
    [ApiController]
    //[Authorize]
    public class TransactLimitsController : ControllerBase
    {
        private readonly TransactLimitContext _context;
        private readonly IGlobalLimitService _globalLimitService;
        private readonly ILimitService _limitService;
        public TransactLimitsController(TransactLimitContext context, IGlobalLimitService globalService, ILimitService limitService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _globalLimitService = globalService ?? throw new ArgumentNullException(nameof(globalService));
            _limitService = limitService ?? throw new ArgumentNullException(nameof(limitService));
        }

        [Route("getlimit")]
        [HttpPost()]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
         public async Task<IActionResult> GetTransactionLimit([FromBody] LimitRequestParameters requestParameters)
        { //Get Transaction Limit - API Specification
            try
            {
                var success = _limitService.GetTransactionLimit(requestParameters);
                return Ok(success);
            }
            catch (ArgumentException aex)
            {
                return StatusCode(409, new
                {
                    requestParameters.rid,
                    status = new Status()
                    {
                        code = "1",
                        description = "Conflict"
                    }
                });
            }
            catch (Exception ex)
            { // 500 Internal Server Error
                return StatusCode(500, new
                {
                    requestParameters.rid,
                    status = new Status()
                    {
                        code = "1",
                        description = "internal server error"
                    }
                });
            }
        }

        [Route("transactionlimit")]
        [HttpPost()]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateTransactionLimit([FromBody] TransactionLimit requestParameters)
        { // Create transaction Limit - API Specification
            try
            {
                var success = _limitService.CreateLimit(requestParameters);
                return Ok(success);
            }
            catch (ArgumentException aex)
            {
                return StatusCode(409, new
                {
                    requestParameters.rid,
                    status = new Status()
                    {
                        code = "1",
                        description = "Conflict"
                    }
                });
            }
            catch (Exception ex)
            { // 500 Internal Server Error
                return StatusCode(500, new
                {
                    requestParameters.rid,
                    status = new Status()
                    {
                        code = "1",
                        description = "internal server error"
                    }
                });
            }
        }

        [Route("globallimit")]
        [HttpPost()]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateGlobalLimit([FromBody] GlobalLimitRequestParameters requestParameters)
        { // Create Global Limit - API Specification
            try
            {
                var result = _globalLimitService.SaveGlobalLimit(requestParameters);
                return Ok(result);
            }
            catch (ArgumentException ex)
            {
                return StatusCode(409, new
                {
                    requestParameters.rid,
                    status = new Status()
                    {
                        code = "1",
                        description = "Conflict"
                    }
                });
            }
            catch (Exception aex)
            { // 500 Internal Server Error
                return StatusCode(500, new
                {
                    requestParameters.rid,
                    status = new Status()
                    {
                        code = "1",
                        description = "internal server error"
                    }
                });
            }
        }
    }
}
