# Overview
The requirement exists to develop a Web Api that will
* Create A Transaction Limit
* Create A Global Limit
* Get A Transaction Limit


The Web Api was been developed using .Net Core 3.1, C# and Visual Studio Code v1.45.0

# Database
An in-Memory database was used to add data to the Database via the "Create Global Limit" and "Create Transaction Limit" as detailed in the API Specification.

# Presentation
I have included Swagger to make supply documentation, and providing a testing platform for the end user.

# Testing
Using Visual Studio Code, press "Ctrl" + F5".
Using the terminal in Visual Studio Code, execute "dotnet run".
The API can be tested with postman as well.

# Installation
Clone repo using:
* https://gitlab.com/equitybank/eqbank-cameron.git
* git@gitlab.com:equitybank/eqbank-cameron.git


# Endpoint to Test
https://localhost:5001/index.html