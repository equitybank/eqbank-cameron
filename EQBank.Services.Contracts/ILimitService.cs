using EQBank.Core.Models;

namespace EQBank.Services.Contracts
{
    public interface ILimitService
    {
         public object CreateLimit(TransactionLimit transactionLimit);
         public object GetTransactionLimit(LimitRequestParameters transactionLimit);
    }
}