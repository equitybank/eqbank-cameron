using EQBank.Core.Models;

namespace EQBank.Services.Contracts
{
    public interface IGlobalLimitService
    {
        public object SaveGlobalLimit(GlobalLimitRequestParameters requestParameters);
    }
}
